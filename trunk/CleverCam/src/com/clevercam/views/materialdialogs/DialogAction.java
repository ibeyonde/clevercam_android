package com.clevercam.views.materialdialogs;

/**
 * @author Aidan Follestad (afollestad)
 */
public enum DialogAction {
    POSITIVE,
    NEUTRAL,
    NEGATIVE
}
