package com.clevercam.views.materialdialogs;

/**
 * @author Aidan Follestad (afollestad)
 */
public enum Theme {
    LIGHT, DARK
}