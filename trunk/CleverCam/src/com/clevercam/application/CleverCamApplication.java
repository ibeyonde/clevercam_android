package com.clevercam.application;

import android.app.Application;
import android.content.Context;

import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.base.api.AsyncHttpHelper;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.network.ConnectionManager;

/**
 * Created by Shashi on 12/10/14.
 */
public class CleverCamApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getBaseContext();
        DeviceManager.initialize(context);              // Initialize DeviceManager
        ConnectionManager.initialize(context);          // Initializing Connection manager
        CleverCamDataHolder.initialize(context);
        AsyncHttpHelper.initialize(context);
    }
}
