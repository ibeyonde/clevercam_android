package com.clevercam.activities;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.clevercam.R;
import com.clevercam.adapters.DrawerListAdapter;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.network.ConnectionManager;
import com.clevercam.base.receivers.WiFiConnectionReceiver;
import com.clevercam.fragments.DiscoverDeviceFragment;
import com.clevercam.fragments.SettingsFragment;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.views.materialdialogs.MaterialDialog;


public class MainActivity extends BaseActivity implements AdapterView.OnItemClickListener, View.OnClickListener{

    private final static String TAG = MainActivity.class.getName();

    private ImageView mReconnectIv;
    private DrawerLayout mNavigationDrawer;
    private ListView mDrawerListView;
    private Handler mWiFiHandler;
    private Handler mVideoViewHandler;

    private WiFiConnectionReceiver mWiFiConnectionReceiver = null;
    private static final String CONNECTIVITY_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private static final String CONNECTIVITY_WIFI_STATE_CHANGE_ACTION = "android.net.wifi.WIFI_STATE_CHANGED";

    private boolean mIsDeviceConfigured;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        setIdsToViews();
        setUpUI();
        setAdapters();
        setListnersToViews();
        setupWiFiReceivers();
        showStartDiscoveryDialog();
    }


    public void setImageError(){
        mVideoViewHandler.post(new Runnable() {
            @Override
            public void run() {
                setTitle("Error reading Image...");
                return;
            }
        });
    }

    /**
     * Method to setup broadcast receivers
     */
    private void setupWiFiReceivers() {
        mWiFiHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                int wifiState = msg.what;
                switch (wifiState){
                    case CleverCamConstants.WIFI_DISCONNECTED:
                        if(!ConnectionManager.getInstance().checkWiFiConnection()) {  // If the device Wifi is not available show dialog to turn on Wifi
                            turnOnWiFiDialog();
                        }
                        break;
                }
            }
        };
        mWiFiConnectionReceiver = new WiFiConnectionReceiver(mWiFiHandler);
        registerWiFiBroadCastReceiver();
    }

    public void turnOnWiFiDialog(){
        final MaterialDialog.Builder mDialogBuilder = new MaterialDialog.Builder(this);
        mDialogBuilder.title(R.string.dialog_wifi_turn_on);
        mDialogBuilder.content(R.string.dialog_wifi_turn_on_content);
        mDialogBuilder.positiveText(R.string.dialog_Ok);
        mDialogBuilder.callback(new MaterialDialog.SimpleCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                dialog.dismiss();
                navigateToSettings();
                mDialogBuilder.autoDismiss(true);
            }
        })
        .build();
        mDialogBuilder.show();
    }

    public void navigateToSettings(){
        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
    }


    private void init() {
        mVideoViewHandler = new Handler();
    }

    private void setListnersToViews() {
       mDrawerListView.setOnItemClickListener(this);
       mReconnectIv.setOnClickListener(this);
    }

    private void setAdapters() {
        mDrawerListView.setAdapter(new DrawerListAdapter(getApplicationContext(), CleverCamConstants.getDrawerItems()));
    }

    @Override
    protected void onResume() {
        mIsDeviceConfigured = DeviceManager.getInstance().isDeviceConfigured();
        super.onResume();
    }

    /**
     * This method will show the dialog if the device is not configured, mostly this will be at first time
     */
    private void showStartDiscoveryDialog() {
        boolean isNetworkConnected = ConnectionManager.getInstance().checkWiFiConnection();
        if(isNetworkConnected) {
            if(!DeviceManager.getInstance().isDeviceConfigured()) {
                final MaterialDialog.Builder dialogBuilder = new MaterialDialog.Builder(this);
                dialogBuilder.title(R.string.dialog_title_discover);
                dialogBuilder.content(R.string.dialod_discover_device_content);
                dialogBuilder.positiveText(R.string.dialog_Ok);
                dialogBuilder.icon(getResources().getDrawable(R.drawable.ic_launcher));
                dialogBuilder.callback(new MaterialDialog.SimpleCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        CleverCamLogger.infoLog(TAG, "showStartDiscoveryDialog() :: Launching discover device fragment");
                        launchFragment(new DiscoverDeviceFragment(), false);
                    }
                })
                .build();
                MaterialDialog meterialDialog = dialogBuilder.show();
                meterialDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;  // Set animations
                meterialDialog.setCancelable(false);
            }else{
                CleverCamLogger.infoLog(TAG, "showStartDiscoveryDialog() :: Launching discover device fragment");
                launchFragment(new DiscoverDeviceFragment(), false);
            }
        }else{
            turnOnWiFiDialog();
        }
    }

    private void setUpUI() {
        setUpActionBar();
        mNavigationDrawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);
    }

    private void setUpActionBar() {
        setActionBarIcon(R.drawable.ic_drawer);
        setActionBarTitile(getString(R.string.app_name));
    }

    private void setIdsToViews() {
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerListView = (ListView) findViewById(R.id.drawer_lv);
        mReconnectIv = (ImageView)getToolBar().findViewById(R.id.ab_reconnect_iv);
    }


    @Override protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        CleverCamLogger.infoLog(TAG, "onActivityResult :: Result code .... "+resultCode);
        if(resultCode == 0){
            if(ConnectionManager.getInstance().checkWiFiConnection())
                showStartDiscoveryDialog();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
             //   if(mIsDeviceConfigured) {
                    mNavigationDrawer.openDrawer(Gravity.START);
              //  }
              //  else
              //     showShortToast(getString(R.string.device_not_configured_help_text));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedCategory = (String)parent.getAdapter().getItem(position);
        CleverCamLogger.infoLog(TAG, "Selected Item is " + selectedCategory);
        CleverCamLogger.infoLog(TAG, "Position of the items "+position);
      //  if(mIsDeviceConfigured) {               // If the device is configured then only we are alowing user to go further
            switch (position){
                case 0:
                    launchViewCameraScreen();
                break;
                case 1:
                break;
                case 2:
                break;
                case 3:
                    setActionBarTitile(CleverCamConstants.SETTINGS_LABEL);
                    launchFragment(new SettingsFragment(), false);
                break;
     //       }
        }
        mNavigationDrawer.closeDrawers();
    }

    private void launchViewCameraScreen() {
        setActionBarTitile(CleverCamConstants.VIEW_CAMERA_LABEL);
       // launchFragment(new ViewCameraFragment(), false);
        Intent viewCameraIntent = new Intent(this, ViewCameraActivity.class);
        startActivity(viewCameraIntent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ab_reconnect_iv:
               // DeviceManager.getInstance().clearDeviceData();
                CleverCamLogger.infoLog(TAG, "onClick() :: Retry discovery");
                launchFragment(new DiscoverDeviceFragment(), false);
            break;
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mWiFiConnectionReceiver);
        super.onDestroy();
    }

  /**
     * Method to registerWiFi broadcast receiver
     */
    public void registerWiFiBroadCastReceiver(){
        IntentFilter wifiFilter = new IntentFilter();
        wifiFilter.addAction(CONNECTIVITY_CHANGE_ACTION);
        wifiFilter.addAction(CONNECTIVITY_WIFI_STATE_CHANGE_ACTION);
        registerReceiver(mWiFiConnectionReceiver, wifiFilter);
    }
}


