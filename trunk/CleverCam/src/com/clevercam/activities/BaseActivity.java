/*
 * Copyright (C) 2014 
 *  
 */

package com.clevercam.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.clevercam.R;
import com.clevercam.utils.CleverCamLogger;

/**
 * @author Shashi
 */

public abstract class BaseActivity extends ActionBarActivity {


    private Toolbar mToolBar;

    private static String TAG = BaseActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolBar != null) {
            setSupportActionBar(mToolBar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected abstract int getLayoutResource();

    protected void setActionBarIcon(int iconRes) {
        mToolBar.setNavigationIcon(iconRes);
    }


    /**
     * Method to show short toast
     * @param toastString
     */
    protected void showShortToast(String toastString){
        Toast.makeText(this, toastString, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method to show Long toast
     * @param toastString
     */
    protected void showLongToast(String toastString){
        Toast.makeText(this, toastString, Toast.LENGTH_SHORT).show();
    }

    protected void setActionBarTitile(String title){
        mToolBar.setTitle(title);
        TextView abTitle = (TextView)mToolBar.findViewById(R.id.ab_title_tv);
        abTitle.setText(title);
    }
	/**
	 * To navigate to other fragment
	 * @param fragment
	 * @param keepFragmentInBackStack
	 */
	protected void launchFragment(Fragment fragment, boolean keepFragmentInBackStack) {
		CleverCamLogger.infoLog(TAG, "Launching fragment:: "+ fragment.getClass().getSimpleName());
		FragmentManager fragmentManager = getFragmentManager();
		if(fragmentManager!=null){
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getName());
			if (keepFragmentInBackStack)
				fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
			// app is will crash with commit operation after savedInstanceState.
			// http://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa

			// FIXME : need to identify the best solution between
			// commitAllowingStateLoss VS removing super.onSaveInstanceState in
			// onSaveInstanceState
			fragmentTransaction.commitAllowingStateLoss();
		}
	}

    /**
     * Method to get toolbar
     * @return mToolBar
     */
    protected Toolbar getToolBar(){
        return  mToolBar;
    }



}


