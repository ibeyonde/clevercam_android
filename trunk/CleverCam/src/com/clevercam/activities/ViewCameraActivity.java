/**
 * 
 */
package com.clevercam.activities;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.camera.simplemjpeg.MjpegInputStream;
import com.camera.simplemjpeg.MjpegView;
import com.clevercam.R;
import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.model.AvailableDevicesModel;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.views.materialdialogs.MaterialDialog;

/**
 * This fragment will show all the the video feeds which are connected in to the device
 * @author Shashi
 *
 */
public class ViewCameraActivity extends Activity {

	private final static String TAG = ViewCameraActivity.class.getName();
    private MjpegView mv;

    private List<AvailableDevicesModel> mAvailableDevicesList;
    private String[] mRemoteIPArray;
    private String videoStreamURL = null;
    private boolean suspending = false;
    private Handler handler ;

    public ViewCameraActivity(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmenr_view_camera);
        init();
        getBundleData();
        setIdsToViews();
        showListOfDevicesAvaialbleDialog();
        if(mv != null){
            mv.setResolution(640, 480);
        }
    }

    private void showListOfDevicesAvaialbleDialog() {
        ArrayAdapter <String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mRemoteIPArray);
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Available devices")
                .contentColorRes(android.R.color.black)
                .adapter(arrayAdapter)
                .build();

        ListView listView = dialog.getListView();
        if (listView != null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                	DeviceManager.getInstance().setDeviceConfigurationState(true);   //This method will set the state of the device connected to wifi true
                    startPrimaryCamera((String)parent.getItemAtPosition(position));
                    dialog.dismiss();
                }
            });
        }

        dialog.show();
    }

    private void init() {
        handler = new Handler();
    }

    public void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        if(mv!=null){
            if(suspending){
                new DoRead().execute(videoStreamURL);
                suspending = false;
            }
        }

    }

    public void setImageError(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                setTitle("Image time out!");
                return;
            }
        });
    }

    public void onDestroy() {
        Log.d(TAG,"onDestroy()");

        if(mv!=null){
            mv.freeCameraMemory();
        }

        super.onDestroy();
    }

    public void onPause() {
        Log.d(TAG,"onPause()");
        super.onPause();
        if(mv!=null){
            if(mv.isStreaming()){
                mv.stopPlayback();
                suspending = true;
            }
        }
    }


    private void startPrimaryCamera(String ip) {
        CleverCamLogger.infoLog(TAG, "URL ---------> "+String.format("http://%s:7567/?action=stream", ip));
        videoStreamURL = String.format("http://%s:7567/?action=stream", ip);
        new DoRead().execute(videoStreamURL);
    }

    public class DoRead extends AsyncTask<String, Void, MjpegInputStream> {
        protected MjpegInputStream doInBackground(String... url) {
            //TODO: if camera has authentication deal with it and don't just not work
            HttpResponse res = null;
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpParams httpParams = httpclient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 5 * 1000);
            HttpConnectionParams.setSoTimeout(httpParams, 5*1000);
            Log.d(TAG, "1. Sending http request");
            try {
                res = httpclient.execute(new HttpGet(URI.create(url[0])));
                Log.d(TAG, "2. Request finished, status = " + res.getStatusLine().getStatusCode());
                if(res.getStatusLine().getStatusCode()==401){
                    //You must turn off camera User Access Control before this will work
                    return null;
                }
                return new MjpegInputStream(res.getEntity().getContent());
            } catch (ClientProtocolException e) {

                    e.printStackTrace();
                    Log.d(TAG, "Request failed-ClientProtocolException", e);

                //Error connecting to camera
            } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "Request failed-IOException", e);
                }
                //Error connecting to camera

            return null;
        }

        protected void onPostExecute(MjpegInputStream result) {
            mv.setSource(result);
            if(result!=null){
                result.setSkip(1);
                setTitle(R.string.app_name);
            }else{
                setTitle("Camera disconnected!!.");
            }
            mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
            mv.showFps(true);
        }
    }

    private void getBundleData() {
        List<AvailableDevicesModel> availaDeviceList = CleverCamDataHolder.getInstance().getAvailableDevices();
        if(availaDeviceList != null) {
            mAvailableDevicesList = availaDeviceList;
            mRemoteIPArray = new String[mAvailableDevicesList.size()];
            for (int i = 0; i < mAvailableDevicesList.size(); i++) {
                AvailableDevicesModel availDevices = mAvailableDevicesList.get(i);
                mRemoteIPArray[i] = availDevices.remoteIP;
                CleverCamLogger.infoLog(TAG, "Device IP : " + availDevices.remoteIP + "\n Device Name : " + availDevices.deviceName + "\n Capabilities : " + availDevices.capabilities);
            }
        }
    }

    /**
     * Setting ids to respective views
     *
     */
    private void setIdsToViews() {
        mv = (MjpegView)findViewById(R.id.view_camera_main_video_stream_vv);
    }
}
