package com.clevercam.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.clevercam.R;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.network.ConnectionManager;
import com.clevercam.utils.views.ProgressWheel;


/**
 * Starting point for CleverCam app.
 * @author Shashi
 */

public class SplashScreenActivity extends BaseActivity{

    private ProgressWheel progressWheel;

    private static String TAG = SplashScreenActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
        init();
        setUpIdsToViews();
        setUpUI();
		setCountdownTimer();
	}

    private void init() {
        boolean isDeviceConfigured 		=	DeviceManager.getInstance().isDeviceConfigured();
        ConnectionManager connManager	=	ConnectionManager.getInstance();
        if (!isDeviceConfigured) {                                          // If the device is not configured
            if (connManager.checkWiFiConnection()) {    // Check wheather WIFI is turned on  {
            	connManager.assignIP();                 // Ping available IPs
            }else{
                showShortToast(getString(R.string.splash_turn_on_wifi_txt));
            }
        }else{
        	showShortToast(getString(R.string.splash_configure_wifi_txt));
        }
    }

    private void setUpUI() {
        progressWheel.spin();
    }

    private void setUpIdsToViews() {
        progressWheel = (ProgressWheel) findViewById(R.id.splash_progress_wheel);
    }

    private void setCountdownTimer() {
		new CountDownTimer(3000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
			}
			@Override
			public void onFinish() {
				startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                progressWheel.stopSpinning();
				finish();
			}
		}.start();
	}

    protected int getLayoutResource(){
        return R.layout.splash_screen;
    }
}

