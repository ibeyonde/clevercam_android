package com.clevercam.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Shashi on 12/16/14.
 */
public class URLHelper {

	private static final String TAG = URLHelper.class.getName();

	private static final AtomicReference<URLHelper> sInstance = new AtomicReference<URLHelper>();

	private static final String ADHOC_BASE_URL = "http://192.168.97.1";

	private static final String HTTP_PREFIX = "http://";

	private static final String URL_ENCODING_TYPE = "UTF-8";

	public static URLHelper getInstance() {
		if (sInstance.get() == null){
			sInstance.set(new URLHelper());
		}
		return sInstance.get();
	}

	private URLHelper() {

	}


	/**
	 * Method to get the URL to configure Wifi once the device is connected in Adhoc mode, this is typically used when we do first time setup
	 * @param ssid
	 * @param password
	 * @return configureWifiURLWithDHCP
	 */
	public String getConfigWiFiURL(String ssid, String password){
		// http://192.168.79.1/wifi.php?name=<SSID>&password=<PSWD>
		return  String.format("%s/wifi.php?name=%s&password=%s", ADHOC_BASE_URL, encodeURL(ssid), encodeURL(password));
	}

	/**
	 *
	 * @param ssid
	 * @param password
	 * @param gateway
	 * @param netmask
	 * @return configureWifiURLWithStaticIP
	 */
	public String getConfigURLUsingStaticIP(String ssid, String password, String gateway, String netmask, String staticIP){
		//  http://<ip>/wifi.php?name=<SSID>&password=<PSWD>&ip=<IP>&gateway=<GATEWAY>&netmask=“<NETMASK>”
		if(!password.isEmpty()) {
			return String.format("%s/wifi.php?name=%s&password=%s&ip=%s&gateway=%s&netmask=%s", ADHOC_BASE_URL, encodeURL(ssid), encodeURL(password), encodeURL(staticIP), encodeURL(gateway), encodeURL(netmask));
		}else{
			return String.format("%s/wifi.php?name=%s&ip=%s&gateway=%s&netmask=%s", ADHOC_BASE_URL, encodeURL(ssid), encodeURL(staticIP), encodeURL(gateway), encodeURL(netmask));

		}
	}

	/**
	 *
	 * @param ssid
	 * @param password
	 * @param gateway
	 * @param netmask
	 * @return configureWifiURLWithStaticIP
	 */
	public String getUUIDURL(){
		//  http://<ip>/wifi.php?name=<SSID>&password=<PSWD>&ip=<IP>&gateway=<GATEWAY>&netmask=“<NETMASK>”

		return String.format("%s/uuid.php", ADHOC_BASE_URL);
	}



	/**
	 * Method to get motion URL, this should be called once the device is connected in Adhoc or WiFi mode.
	 * @param wifiIP
	 * @return motionURL
	 */
	public String getMotionURL(String wifiIP) {
		//http://<ip>/json/motion.php
		return String.format("%s%s/json/motion.php", HTTP_PREFIX,wifiIP);
	}

	/**
	 * Method to get motion temperature, this should be called once the device is connected in Adhoc or WiFi mode.
	 * @param wifiIP
	 * @return temperatureURL
	 */
	public String getTemperatureURL(String wifiIP) {
		//http://<ip>/json/temp.php
		return String.format("%s%s/json/temp.php", HTTP_PREFIX, wifiIP);
	}

	/**
	 * Method to get snapshot URL, we can take snapshot only when the Device is connected
	 * @param wifiIP
	 * @return snapShotURL
	 */
	public String getSnapShotURL(String wifiIP){
		//http://<ip>:7567/?action=snapshot
		return String.format("%s%s:7567/?action=snapshot", HTTP_PREFIX,wifiIP);
	}

	/**
	 * Method to get the Video Stream URL, this should be called only when
	 * @param wifiIP
	 * @return videoFeedURL
	 */
	public String getVideoFeed(String wifiIP){
		//http://<ip>:7567/?action=stream
		return String.format("%s%s:7567/?action=stream", HTTP_PREFIX,wifiIP);
	}

	/**
	 * This method will encode supplied URLs using UTF based encoding
	 * @param url
	 * @return
	 */
	private String encodeURL(String url){
		CleverCamLogger.infoLog(TAG, "encodeURL() :: Encoding URL params: "+ url);
		String encodedURL = null;
		try {
			encodedURL = URLEncoder.encode(url, URL_ENCODING_TYPE);
			CleverCamLogger.infoLog(TAG, "encodeURL() :: After encoded URL is params : "+ encodedURL);
		} catch (UnsupportedEncodingException e) {
			CleverCamLogger.errorLog(TAG, "encodeURL() :: Exception while encoding URL \n URL is :: "+url,e);
		}
		return  encodedURL;
	}

}
