package com.clevercam.utils;

import java.util.Date;

import android.util.Log;

public class CleverCamLogger {
	public static final String LOG_TAG = "CleverCam";
	public static final boolean logsRequired = true;
	private static final boolean isInProductionMode = false;

	public static boolean logsRequired(){
		return logsRequired;
	}

	public static void infoLog(String tag, String msg) {
		if(!isInProductionMode){
			if (logsRequired()) {
				logForTestMode(tag, msg);
				return;
			}
			if(msg != null)
				Log.d(tag, msg);
		}
	}

	private static void logForTestMode(String tag, String msg, Exception... optional) {
		String messag = null;
		if(!isInProductionMode){
			Exception e = optional == null || optional.length == 0 ? null : optional[0];
			if (e == null) {
				System.out.println(String.format("%s - %s - %s", new Date().toString(), tag, msg));
				messag = String.format("%s - %s - %s", new Date().toString(), tag, msg);
			}
			else {
				System.out.println(String.format("%s - %s - %s  Error message = %s", new Date().toString(), tag, msg, e.getCause() == null ? e.getMessage() : e.getCause().getMessage()));
				messag = String.format("%s - %s - %s  Error message = %s", new Date().toString(), tag, msg, e.getCause() == null ? e.getMessage() : e.getCause().getMessage());
			}
		}
		
	}

	public static void errorLog(String tag, String msg, Exception... optional) {
		String message = null;
		if(!isInProductionMode){
			if (logsRequired()) {
				logForTestMode(tag, msg, optional);
				return;
			}
			Exception e = optional == null || optional.length == 0 ? null : optional[0];
			if (e == null) {
				Log.e(tag, String.format("%s  Error message = %s  Exception e is null.  ", msg, ""));
				message = String.format("%s : %s  Error message = %s  Exception e is null.  ", tag, msg, "");
			}
			else {
				Log.e(tag, String.format("%s  Error message = %s", msg, e.getCause() == null ? e.getMessage() : e.getCause().getMessage()));
				message = String.format(" %s : %s  Error message = %s", tag, msg, e.getCause() == null ? e.getMessage() : e.getCause().getMessage());
			}
			
		}
	}
}
