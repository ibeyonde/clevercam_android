/**
 * 
 */
package com.clevercam.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shashi
 *
 */
public class CleverCamConstants {

	public static final String EMPTY_STRING 		     = 	"";

	public static final String VIEW_CAMERA_LABEL         = 	"View Camera";
	public static final String MOTION_ALERT_LABEL	     = 	"Motion Alert";
	public static final String TEMP_AND_HUMID_LABEL      = 	"Temp & Humid";
	public static final String SETTINGS_LABEL  		     = 	"Settings";

	public static final String CLEVER_CAM_PREF =  "ClearCamSharedPref";

	//Response Code
	public static final int RESPONSE_SUCCESS             =  200;
	public static final int RESPONSE_FAILURE             =  404;


	//Shared Pref Keys
	public static final String IS_DEVICE_DISCOVERD_KEY   =  "CLEAR_CAM_DISCOVERD";
	public static final String DEVICE_NAME_KEY           =  "DEVICE_NAME";
	public static final String AVILABLE_DEVICE_LIST_KEY  =   "AvaiableDeviceListKey";
	public static final String AVAILABLE_STATIC_IP       =   "AvailStaticIPKey";
	public static final String GATEWAY_KEY               =   "GatewayKey";
	public static final String SSID_KEY                  =   "SSIDKey";
	public static final String UUID_KEY                  =   "UUIDKey";

	public static final String COLON                     =  ":";
	public static final java.lang.String COMMA           =  ",";
	public static final String DOT                       =  ".";



	public static List<String> getDrawerItems(){
		List<String> drawerList = new ArrayList<String>();
		drawerList.add(VIEW_CAMERA_LABEL);
		drawerList.add(MOTION_ALERT_LABEL);
		drawerList.add(TEMP_AND_HUMID_LABEL);
		drawerList.add(SETTINGS_LABEL);
		return drawerList;
	}

	public static final String CLEVER_CAM_IDEFIFIER           =   "Clever";
	//public static final String CLEVER_CAM_IDEFIFIER           =   "CleverCam";
	//public static final String CLEVER_CAM_IDEFIFIER         =   "TableTop";

	public static final int UDP_DISCOVERY_PORT                =    6999;


	public static final int WIFI_DISCONNECTED                 =    0;


	//CleverCam broadcast Identifier
	public static final String CLEVER_CAM_BROADCAST_IDENTIFIER	= "[#%^!$]";

			public static class REQUEST_KEYS{
		public static String WIFI_CONFIG_SSID_KEY = "ssid";
		public static String WIFI_CONFIG_PASSWORD_KEY = "password";
		public static String URL_KEY = "password";

	}

}
