/**
 * 
 */
package com.clevercam.base.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.clevercam.utils.CleverCamConstants;

/**
 * @author shashi
 *
 */
public class SharedPreferenceManager {

	public static void saveString(Context context, String key, String value) {
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void saveInt(Context context, String key, int value) {
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void saveBool(Context context, String key, boolean value) {
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}


	public static String getString(Context context, String key, String defValue){
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		return pref.getString(key, defValue);	
	}

	public static int getInt(Context context, String key, int defValue){
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		return pref.getInt(key, defValue);
	}

	public static boolean getBool(Context context, String key, boolean defValue){
		SharedPreferences pref = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		return pref.getBoolean(key, defValue);	
	}


	public static void clearAllPref(Context context){
		SharedPreferences settings = context.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		settings.edit().clear().commit();
	}


}
