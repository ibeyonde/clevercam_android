package com.clevercam.base.managers;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.os.Handler;

import com.clevercam.base.api.AsyncAPIResponseHandler;
import com.clevercam.base.api.AsyncHttpHelper;
import com.clevercam.base.model.WiFiConfigModel;
import com.clevercam.base.network.ConnectionManager;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.utils.URLHelper;
import com.loopj.android.http.RequestParams;

/**
 * This class is responsible to handle the device operations
 * Created by Shashi on 12/10/14.
 */
public class DeviceManager implements IDeviceManager {

	private static final String TAG = DeviceManager.class.getName();

	private static final AtomicReference<IDeviceManager> sInstance = new AtomicReference<IDeviceManager>();
	private final Context mContext;


	public static void initialize(final Context applicationContext) {
		sInstance.compareAndSet(null, new DeviceManager(applicationContext));
	}

	public static IDeviceManager getInstance(){
		return sInstance.get();
	}

	private DeviceManager(final Context context) {
		mContext = context;
	}


	/**
	 * This method will return the whether the CLeverCam configured previously or not,
	 * this will be set to true one the device is configured successfully with Wifi
	 * @return isDeviceConfigured
	 */
	@Override
	public boolean isDeviceConfigured(){
		CleverCamLogger.infoLog(TAG, "isDeviceConfigured() :: Checking whether the device is configured or not");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		boolean isDeviceConfigured = prefs.getBoolean(CleverCamConstants.IS_DEVICE_DISCOVERD_KEY, false);
		CleverCamLogger.infoLog(TAG, "isDeviceConfigured() :: "+isDeviceConfigured);
		return  isDeviceConfigured;
	}

	/**
	 * When the device is connected we need to call this method to persist the value
	 */
	@Override
	public void setDeviceConfigurationState(boolean isDeviceSuccessfullyDescoverdAndConnectedToWifi){
		CleverCamLogger.infoLog(TAG, "setDeviceConfigurationState)_ :: setting device configuration state "+isDeviceSuccessfullyDescoverdAndConnectedToWifi);
		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putBoolean(CleverCamConstants.IS_DEVICE_DISCOVERD_KEY, isDeviceSuccessfullyDescoverdAndConnectedToWifi);
		editor.commit();
	}

	@Override
	public void setMasterDeviceStaticIP(String staticIP) {
		CleverCamLogger.infoLog(TAG, "setMasterDeviceStaticIP()_ :: setting static IP for device which is available");
		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putString(CleverCamConstants.AVAILABLE_STATIC_IP, staticIP);
		editor.commit();
	}

	@Override
	public String getStaticIPAssignedForMasterDevice() {
		CleverCamLogger.infoLog(TAG, "getStaticIPAssignedForMasterDevice() :: Getting static IP assigned to device");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		String staticIPAssignedToDeivce = prefs.getString(CleverCamConstants.AVAILABLE_STATIC_IP, null);
		CleverCamLogger.infoLog(TAG, "getStaticIPAssignedForMasterDevice() :: static IP  "+staticIPAssignedToDeivce);
		return  staticIPAssignedToDeivce;
	}


	/**
	 * Method to check weather the device is connected in ADHocMode or not
	 * @return isDeviceInAdHocMode
	 */
	@Override
	public boolean isDeviceInAdhocMode() {
		boolean isDeviceInAdhocMode = false;
		return isDeviceInAdhocMode;
	}

	/**
	 * Method to connect the device in Adhoc mode, Adhoc mode will connects to a a static ip i.e 192.168.79.1
	 */
	@Override
	public boolean connectInAdhocMode() {
		CleverCamLogger.infoLog(TAG, "connectInAdhocMode() :: Connecting Device in Adhocmode, discovering clevercam");
		String deviceName = DeviceManager.getInstance().getDeviceName();
		CleverCamLogger.infoLog(TAG, "connectInAdhocMode() :: deviceName - "+deviceName);
		boolean isCleverCamFound = false;
		List<ScanResult> wifiAccesspointList =  ConnectionManager.getInstance().scanAvailableWiFi();
		for (ScanResult wifiScanResult: wifiAccesspointList){       // Scan for the clear cam
			if((wifiScanResult.SSID).contains(deviceName) || (wifiScanResult.SSID).contains("ibe")){
				isCleverCamFound = true;
				CleverCamLogger.infoLog(TAG, "CleverCam found and SSID : "+wifiScanResult.SSID);
				break;
			}
		}
		if (isCleverCamFound){   // Clevercam is found lets connect to it
			ConnectionManager.getInstance().connectToWiFi(deviceName);
		}else{
			isCleverCamFound = false;
			return false;
		}
		return isCleverCamFound;
	}

	@Override
	public void setDeviceName(String deviceName) {
		CleverCamLogger.infoLog(TAG, "setDeviceConfigurationState)_ :: setting device name :"+ deviceName);
		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putString(CleverCamConstants.DEVICE_NAME_KEY, deviceName);
		editor.commit();

	}

	@Override
	public String getDeviceName() {
		CleverCamLogger.infoLog(TAG, "getDeviceName() from preferences ");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		String deviceName = prefs.getString(CleverCamConstants.DEVICE_NAME_KEY, CleverCamConstants.CLEVER_CAM_IDEFIFIER);
		CleverCamLogger.infoLog(TAG, "getDeviceName() :: "+deviceName);
		return deviceName;
	}

	/**
	 * Method to send Wifi details to device, this should be called only once the device is connected in adhoc mode mostly first time.
	 * this will be DHCP
	 * @param wifiConfigParams
	 * @param responseMessageHandler
	 */
	@Override
	public void sendWiFIDetailsToDeviceDHCP(WiFiConfigModel wifiConfigParams, Handler responseMessageHandler) {
		CleverCamLogger.infoLog(TAG, "sendWiFIDetailsToDevice() :: sendingWiFiDetailsToDevice");
		RequestParams reqParams = new RequestParams();
		CleverCamLogger.infoLog(TAG, "sendWiFIDetailsToDeviceDHCP() :: Configure WiFI URL : " + URLHelper.getInstance().getConfigWiFiURL(wifiConfigParams.ssid, wifiConfigParams.password));
		AsyncHttpHelper.getInstance().get(URLHelper.getInstance().getConfigWiFiURL(wifiConfigParams.ssid, wifiConfigParams.password),reqParams, AsyncAPIResponseHandler.getInstance());
	}

	/**
	 * Method to send Wifi details to the device to configure WIFI, with static IP
	 * @param wificonfigParams
	 * @param handler
	 */
	@Override
	public void sendWiFIDetailsToDeviceByStaticIP(WiFiConfigModel wificonfigParams, Handler handler) {
		CleverCamLogger.infoLog(TAG, "sendWiFIDetailsToDeviceByStaticIP() :: sendWiFIDetailsToDeviceByStaticIP");
		RequestParams reqParams = new RequestParams();
		String configWIFIUsingStaticIPURL = URLHelper.getInstance().getConfigURLUsingStaticIP(wificonfigParams.ssid, wificonfigParams.password, wificonfigParams.gateway, wificonfigParams.netmask, wificonfigParams.staticIP);
		CleverCamLogger.infoLog(TAG, "sendWiFIDetailsToDeviceByStaticIP() :: Configure WiFI URL using static IP : " + configWIFIUsingStaticIPURL);
		AsyncHttpHelper.getInstance().get(configWIFIUsingStaticIPURL,reqParams, AsyncAPIResponseHandler.getInstance());
	}

	/**
	 * Method to send Wifi details to the device to configure WIFI, with static IP
	 * @param wificonfigParams
	 * @param handler
	 */
	@Override
	public void getUUIDOfDevice(Handler handler) {
		CleverCamLogger.infoLog(TAG, "getUUIDOfDevice() :: getUUIDOfDevice");
		RequestParams reqParams = new RequestParams();
		String getUUIDURL = URLHelper.getInstance().getUUIDURL();
		CleverCamLogger.infoLog(TAG, "getUUIDOfDevice() :: getUUIDOfDevice URL is "+getUUIDURL);
		AsyncHttpHelper.getInstance().get(getUUIDURL,reqParams, AsyncAPIResponseHandler.getInstance());
	}

	@Override
	public void clearDeviceData() {
		CleverCamLogger.infoLog(TAG, "clearDeviceData() :: clearingDeviceData...from pref");
		SharedPreferences settings = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		settings.edit().clear().commit();
	}

	/**
	 * Method to get SSID of the wifi network, typically this will be home wifi
	 * @return ssid
	 */
	@Override
	public String getSSID() {
		CleverCamLogger.infoLog(TAG, "getSSID() :: getting SSID for HomeWIFI");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		String ssid  = prefs.getString(CleverCamConstants.SSID_KEY, CleverCamConstants.CLEVER_CAM_IDEFIFIER);
		CleverCamLogger.infoLog(TAG, "getSSID() :: SSID  -> "+ssid);
		return ssid.substring(1,(ssid.length() -1));
	}

	public void testPing() {
		RequestParams reqParams = new RequestParams();
		AsyncHttpHelper.getInstance().get("http://api.twitter.com/1/" ,reqParams, AsyncAPIResponseHandler.getInstance());
	}

	@Override
	public String getStoredUUID() {
		CleverCamLogger.infoLog(TAG, "getStoredUUID() :: getting UUID ");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		String UUID  = prefs.getString(CleverCamConstants.UUID_KEY, "0000-0000");
		CleverCamLogger.infoLog(TAG, "getStoredUUID() :: UUID  -> "+UUID);
		return UUID;
	}

	@Override
	public void setUUIDToStore(String UUID) {
		CleverCamLogger.infoLog(TAG, "setUUIDToStore)_ :: setUUIDToStore ");
		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putString(CleverCamConstants.UUID_KEY, "ibe"+UUID);
		editor.commit();	
	}

}
