package com.clevercam.base.managers;

import android.os.Handler;

import com.clevercam.base.model.WiFiConfigModel;

/**
 * Created by Shashi on 12/13/14.
 */
public interface IDeviceManager {

    public void setDeviceConfigurationState(boolean isDeviceSuccessfullyDescoverdAndConnectedToWifi);

    public void setMasterDeviceStaticIP(String staticIP);

    public String getStaticIPAssignedForMasterDevice();

    public boolean isDeviceConfigured();

    public void getUUIDOfDevice(Handler handler);
    
    public boolean isDeviceInAdhocMode();

    public boolean connectInAdhocMode();

    public void setDeviceName(String deviceName);

    public String getDeviceName();

    public void sendWiFIDetailsToDeviceDHCP(WiFiConfigModel wiFiConfigModel, Handler handler);

    public void sendWiFIDetailsToDeviceByStaticIP(WiFiConfigModel wiFiConfigModel, Handler handler);

    public void clearDeviceData();

    public String getSSID();
    
    public String getStoredUUID();
    
    public void setUUIDToStore(String UUID);

}
