package com.clevercam.base.api;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.http.Header;

import android.os.Handler;
import android.os.Message;

import com.clevercam.base.api.model.ResponseResult;
import com.clevercam.utils.CleverCamLogger;
import com.loopj.android.http.AsyncHttpResponseHandler;

/**
 * Created by Shashi on 12/16/14.
 */
public class AsyncAPIResponseHandler extends AsyncHttpResponseHandler {



    private static final String TAG = AsyncAPIResponseHandler.class.getName();
    private static final int OK_200 = 200;

    private Handler mUIHandler;

    private static final AtomicReference<AsyncAPIResponseHandler> sInstance = new AtomicReference<AsyncAPIResponseHandler>();


    public static AsyncAPIResponseHandler getInstance() {
        CleverCamLogger.infoLog(TAG, "getInstance() :: getting instance for AsyncAPIResponseHandler");
        if (sInstance.get() == null){
            sInstance.set(new AsyncAPIResponseHandler());
        }
        return sInstance.get();
    }

    private AsyncAPIResponseHandler() {

    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        CleverCamLogger.infoLog(TAG, "onFailure() :: We got a failure response from Server and status code is : "+statusCode +"Resp body : "+responseBody);

        ResponseResult responseResult = new ResponseResult();
        responseResult.statusCode = statusCode;
        responseResult.headers = headers;
        responseResult.responseBody = responseBody;

        Message message = new Message();
        message.obj = responseResult;
        mUIHandler.sendMessage(message);

        super.onFailure(statusCode, headers, responseBody, error);

    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        CleverCamLogger.infoLog(TAG, "onSucces() :: We got a success response from Server and status code is : "+statusCode);

        ResponseResult responseResult = new ResponseResult();
        responseResult.statusCode   = statusCode;
        responseResult.headers      = headers;
        responseResult.responseBody = responseBody;
        responseResult.statusCode   = statusCode;

        Message message = new Message();
        message.obj = responseResult;
        mUIHandler.sendMessage(message);

        super.onSuccess(statusCode, headers, responseBody);
    }

    @Override
    public void onStart() {
        CleverCamLogger.infoLog(TAG,"onStart()::starting response");
        super.onStart();
    }

    public void setUIHandler(Handler handler) {
        mUIHandler = handler;
    }
}
