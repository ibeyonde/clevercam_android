package com.clevercam.base.api;

import java.util.concurrent.atomic.AtomicReference;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;

/**
 * Created by Shashi on 12/16/14.
 */
public class AsyncHttpHelper extends AsyncHttpClient {


    private static final String TAG = AsyncHttpHelper.class.getName();
    private static final AtomicReference<AsyncHttpHelper> sInstance = new AtomicReference<AsyncHttpHelper>();
    private final Context mContext;

    public static void initialize(final Context applicationContext) {
        sInstance.compareAndSet(null, new AsyncHttpHelper(applicationContext));
    }

    public static AsyncHttpHelper getInstance() {
        return sInstance.get();
    }

    private AsyncHttpHelper(final Context context) {
        mContext = context;
    }



}
