package com.clevercam.base.api.model;

import org.apache.http.Header;

/**
 * Created by Shashi on 12/18/14.
 */
public class ResponseResult {

    public int statusCode;
    public Header[] headers;
    public byte[] responseBody;
    public Throwable error;
}
