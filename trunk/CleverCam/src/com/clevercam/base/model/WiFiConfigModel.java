package com.clevercam.base.model;

/**
 * Created by shashi on 1/19/15.
 */
public class WiFiConfigModel {

    public String ssid;
    public String password;
    public String gateway;
    public String netmask;
    public String staticIP;
}
