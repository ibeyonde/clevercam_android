package com.clevercam.base.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;

import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;

/**
 * Created by Shashi on 12/15/14.
 */
public class WiFiConnectionReceiver extends BroadcastReceiver{

    private static final String TAG = WiFiConnectionReceiver.class.getName();

    private Handler mHandler;

    public WiFiConnectionReceiver(Handler connectionHandler){
        mHandler = connectionHandler;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String wifiStatus = null;
        ConnectivityManager  connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo network = connectivityManager.getActiveNetworkInfo();

        if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected()) {
                CleverCamLogger.infoLog(TAG, "Wifi is connected: " + String.valueOf(networkInfo));
                CleverCamDataHolder.getInstance().setIsWiFiConnectionState(true);
            }
        } else if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION) || intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI && !networkInfo.isConnected() || !wifiInfo.isAvailable()) {
                wifiStatus = String.valueOf(networkInfo);
                CleverCamDataHolder.getInstance().setIsWiFiConnectionState(false);
                CleverCamLogger.infoLog(TAG, "Wifi is disconnected: " + String.valueOf(networkInfo));
                sendMessageToUI(wifiStatus);
            }
        }
    }

    private void sendMessageToUI(String wifiStatus){
        CleverCamLogger.infoLog(TAG , "WiFi connection state "+wifiStatus);
        Message message = new Message();
        message.what = CleverCamConstants.WIFI_DISCONNECTED;
        mHandler.sendMessage(message);
    }
}
