package com.clevercam.base.network;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;

/**
 * Class is responsible to handle all connections related to CleverCam
 * Created by Shashi on 12/13/14.
 */
public class ConnectionManager {

	private static final String TAG 			= ConnectionManager.class.getName();

	private static final AtomicReference<ConnectionManager> sInstance = new AtomicReference<ConnectionManager>();
	private final Context mContext;
	private static final int DISCOVER_TIMEOUT 	= 20000;

	private int SEARCH_IP_POSTFIX 				= 154;  //This will start ping for available IPs from XXX.XXX.XXX.115

	private static final String STATIC_NETMASK 	= "255.255.255.0";


	public static void initialize(final Context applicationContext) {
		sInstance.compareAndSet(null, new ConnectionManager(applicationContext));
	}

	public static ConnectionManager getInstance() {
		return sInstance.get();
	}

	private ConnectionManager(final Context context) {
		mContext = context;
	}


	/**
	 * Method to know weather the wifi is turned on or not
	 * @return isWifiTurendOn
	 */
	public boolean checkWiFiConnection(){

		boolean isWifiTurendOn = false;
		CleverCamLogger.infoLog(TAG, "checkWiFiConnection() :: Checking WiFi connection....");

		WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		CleverCamLogger.infoLog(TAG, "checkWiFiConnection() :: is wifi enabled "+wifi.isWifiEnabled());
		if(wifi.isWifiEnabled()){
			isWifiTurendOn = true;
			wifi.setWifiEnabled(true);
		}
		CleverCamDataHolder.getInstance().setIsWiFiConnectionState(isWifiTurendOn);
		CleverCamLogger.infoLog(TAG, "checkWiFiConnection Wifi status : "+isWifiTurendOn);
		return isWifiTurendOn;
	}

	/**
	 * Method to assign static available IP, it searches default gateway and creates a static IP, this acts as a
	 * dedicated IP when user reaches home wifi network
	 */
	public boolean assignIP(){
		boolean isIPAssiged		=		false;
		CleverCamLogger.infoLog(TAG, "assignIP() :: start getting  IP ");
		DeviceManager.getInstance().clearDeviceData();
		findGateway();
		findSSID();
		String gatewayOfNetwork = getGateway();
		String[] splitString = gatewayOfNetwork.split("\\"+CleverCamConstants.DOT); // Divide gateway into 3parts
		CleverCamLogger.infoLog(TAG, splitString+" Length "+splitString.length);
		StringBuilder stringBuilder = new StringBuilder();  //Build IP depends on gateway which ends with .115
		String firstParam = splitString[0];
		stringBuilder.append(firstParam);   // as First part of ip starts with /XXX ignore first character
		stringBuilder.append(String.format(".%s",splitString[1]));
		stringBuilder.append(String.format(".%s",splitString[2]));
	//	stringBuilder.append(String.format(".%s",SEARCH_IP_POSTFIX));
		String gatewayString	=	stringBuilder.toString();
		

		Runtime runtime = Runtime.getRuntime();
		try{
			Process  ipAddressPorcess = null;
			for(int i=SEARCH_IP_POSTFIX; i<255 ;i++){
				String staticIP = 	gatewayString + "."+i;
				CleverCamLogger.infoLog(TAG, "assignIP() :: Start pinging static IP  "+staticIP);
				ipAddressPorcess = runtime.exec("/system/bin/ping -c 1 "+staticIP);
				int pingResponse = ipAddressPorcess.waitFor();
				CleverCamLogger.infoLog(TAG, " assignIP() :: Ping response  "+pingResponse);
				if(pingResponse == 1){
					CleverCamLogger.infoLog(TAG, "assignIP() :: we got a positive ping from : "+staticIP +" and it is available");
					DeviceManager.getInstance().setMasterDeviceStaticIP(staticIP);
					isIPAssiged		=		true;
					return isIPAssiged;
				}else{
					if(!staticIP.startsWith("0")){
						CleverCamLogger.infoLog(TAG, "assignIP() :: IP is not available " + staticIP);
						String[] seperatedStrings = staticIP.split("\\"+CleverCamConstants.DOT);

						int increasedIP = Integer.parseInt(seperatedStrings[3]); // Converting string to int for the last digit
						StringBuilder builder  = new StringBuilder();
						builder.append(seperatedStrings[0]);
						builder.append(seperatedStrings[1]);
						builder.append(seperatedStrings[2]);
						builder.append(increasedIP);
						staticIP = builder.toString();
					}else{
						CleverCamLogger.infoLog(TAG, "assignIP() ::Static IP is invalied and it sohuld not start with 0, please check the source of Network connected to");
						isIPAssiged		=		false;
					}
					
				}
			}
		}
		catch (InterruptedException ignore){
			CleverCamLogger.infoLog(TAG, "assignIP() ::  Exception:"+ignore);
		}
		catch (IOException exception){
			CleverCamLogger.infoLog(TAG, "assignIP() ::  Exception:" + exception);
		}
		return isIPAssiged;
	}

	/**
	 * Method to find SSID for connected WIFI
	 */
	private void findSSID() {
		CleverCamLogger.infoLog(TAG, "findSSID() :: Finding SSID for network which is connected");
		WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifiManager.getConnectionInfo ();
		String ssid = info.getSSID ();
		CleverCamLogger.infoLog(TAG, "findSSID() :: SSID of network "+ssid);

		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putString(CleverCamConstants.SSID_KEY, ssid);
		editor.commit();
	}

	/**
	 * Returns the wifi access point list
	 * @return wifiScanList
	 */
	public List<ScanResult> scanAvailableWiFi(){
		CleverCamLogger.infoLog(TAG, "scanAvailableWiFi() :: Scanning WiFi networks available");
		WifiManager mainWifiObj = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		mainWifiObj.startScan();
		List<ScanResult> wifiScanResultList = mainWifiObj.getScanResults();
		CleverCamLogger.infoLog(TAG, "scanAvailableWiFi() :: Number of wifi networks available : "+wifiScanResultList.size());
		return wifiScanResultList;
	}

	/**
	 * Method to receive UDP broadcast packets over 6999
	 * @return broadcastDataList
	 */
	public List<String> receiveUDPPackets(){
		CleverCamLogger.infoLog(TAG, "receiveUDPPackets() :: Start reciving packets on 6999 port");
		String receivedPackets = null;
		DatagramSocket socket = null;
		final List<String> broadcastDataList = new ArrayList<String>();
		try {
			socket = new DatagramSocket(CleverCamConstants.UDP_DISCOVERY_PORT);
			byte[] data = new byte[1024];
			socket.setBroadcast(true);
			DatagramPacket packet = new DatagramPacket(data, data.length,getBroadcastAddress(), CleverCamConstants.UDP_DISCOVERY_PORT);
			socket.send(packet);
			socket.setSoTimeout(DISCOVER_TIMEOUT);
			CleverCamLogger.infoLog(TAG, "receiveUDPPackets() :: broadcast sent to 6999 ready to receive ");

			for(int i=0; i<25 ;i++){    // As we will get some time to discover lets itterate for 25 times to get the cam to be discoverable
				byte[] buf = new byte[1024];
				DatagramPacket receivedPacket = new DatagramPacket(buf, buf.length);
				socket.receive(receivedPacket);
				String packetData = new String(receivedPacket.getData(), 0, receivedPacket.getLength());
				broadcastDataList.add(String.format("%s:%s",receivedPacket.getAddress(), packetData));
				CleverCamLogger.infoLog(TAG, "receiveUDPPackets () Packet data : " + packetData+
						"\n Remote Socket Address: " + receivedPacket.getAddress() + "\n Port :  " + receivedPacket.getPort() );
			}

		} catch (SocketTimeoutException e) {
			CleverCamLogger.errorLog(TAG, "receiveUDPPackets() :: SocketTimedout"+e);
		}catch(Exception e){
			CleverCamLogger.errorLog(TAG, "receiveUDPPackets() :: Exception : "+e);
		}
		socket.disconnect();
		socket.close();
		return broadcastDataList;
	}


	/**
	 * Getting broadcast address which will be used to get/receive UDP packet .
	 * @return ip
	 * @throws IOException
	 */
	private InetAddress getBroadcastAddress() throws IOException {
		CleverCamLogger.infoLog(TAG, "getBroadcastAddress() :: Getting broadcast address ");
		WifiManager wifi = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
		DhcpInfo dhcp = wifi.getDhcpInfo();
		// handle null somehow
		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		CleverCamLogger.infoLog(TAG, "getBroadcastAddress() :: Broadcast address is : "+InetAddress.getByAddress(quads));
		return InetAddress.getByAddress(quads);
	}

	/**
	 * Method to connect WIFI for given SSID
	 * @param ssid
	 */
	public void connectToWiFi(String ssid) {
		CleverCamLogger.infoLog(TAG, "connectToWiFi() :: connecting to the Wifi which has SSID :"+ssid);
		WifiConfiguration conf = new WifiConfiguration();
		conf.SSID = "\"" + ssid + "\"";
		conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
		WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
		wifiManager.addNetwork(conf);
		List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
		for(WifiConfiguration wifiConfiguration : list ) {
			if(wifiConfiguration.SSID != null && wifiConfiguration.SSID.equals("\"" + ssid + "\"")) {
				wifiManager.disconnect();
				wifiManager.enableNetwork(wifiConfiguration.networkId, true);
				wifiManager.reconnect();
				break;
			}
		}
	}

	/**
	 * Method to get default gateway of the connected network
	 * @return gatewayOfNetwork
	 */
	public String getGateway(){
		CleverCamLogger.infoLog(TAG, "getGateway() :: getting gateway");
		SharedPreferences prefs = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE);
		String gateway = prefs.getString(CleverCamConstants.GATEWAY_KEY, null);
		CleverCamLogger.infoLog(TAG, "getGateway() ::  IP  "+gateway);
		return gateway;
	}

	/**
	 * Method to get netmask of the connected network
	 * @return
	 */
	public String getNetmask(){
		CleverCamLogger.infoLog(TAG, "Getting netmask : "+STATIC_NETMASK);
		return  STATIC_NETMASK;
	}


	/**
	 * Method to convert integer to IP
	 * @param intIP
	 * @return ip
	 */
	public String intToIp(int intIP) {
		CleverCamLogger.infoLog(TAG, "intToIp() :: converting int to IP, int is "+intIP);
		String ip = null;
		try{
			byte[] processedBytes = new byte[4];

			for (int k = 0; k < 4; k++) {
				processedBytes[k] = (byte) ((intIP >> k * 8) & 0xFF);
			}
			ip = InetAddress.getByAddress(processedBytes).toString();
			CleverCamLogger.infoLog(TAG, "intToIp() :: Converted IP "+ip);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return  ip;
	}

	/**
	 * Setting gateway for the home/default network
	 */
	public void findGateway(){
		CleverCamLogger.infoLog(TAG, "findGateway() :: gettingDefaultGateWay of connected network");
		String gatewayOfNetwork = null;
		WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);

		DhcpInfo dhcpInfo = wifi.getDhcpInfo();
		gatewayOfNetwork = String.valueOf(intToIp(dhcpInfo.gateway));
		CleverCamLogger.infoLog(TAG, "findGateway() :: Default Gateway is : "+gatewayOfNetwork);

		CleverCamLogger.infoLog(TAG, "findGateway()_ :: Persist gateway");
		SharedPreferences.Editor editor = mContext.getSharedPreferences(CleverCamConstants.CLEVER_CAM_PREF, Context.MODE_PRIVATE).edit();
		editor.putString(CleverCamConstants.GATEWAY_KEY, gatewayOfNetwork.substring(1,gatewayOfNetwork.length())); // to remove "\"
		editor.commit();
	}

}
