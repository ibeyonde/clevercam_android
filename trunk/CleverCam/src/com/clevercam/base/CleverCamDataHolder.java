package com.clevercam.base;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import android.content.Context;

import com.clevercam.base.model.AvailableDevicesModel;

/**
 * Created by Shashi on 12/15/14.
 */
public class CleverCamDataHolder {

    private boolean isWiFiConnected;
    private List<AvailableDevicesModel> mAvailableDevices;

    private static final String TAG = CleverCamDataHolder.class.getName();

    private static final AtomicReference<CleverCamDataHolder> sInstance = new AtomicReference<CleverCamDataHolder>();
    private final Context mContext;

    public static void initialize(final Context applicationContext) {
        sInstance.compareAndSet(null, new CleverCamDataHolder(applicationContext));
    }

    public static CleverCamDataHolder getInstance() {
        return sInstance.get();
    }

    private CleverCamDataHolder(final Context context) {
        mContext = context;
    }

    public void setIsWiFiConnectionState(boolean isWiFiConnected){
       this.isWiFiConnected = isWiFiConnected;
    }

    public boolean getWiFiConnectionStatus(){
        return  isWiFiConnected;
    }


    public void setAvaialbleDeviceList(List<AvailableDevicesModel> availableDeviceList){
        mAvailableDevices = availableDeviceList;
    }

    public List<AvailableDevicesModel> getAvailableDevices(){
        return mAvailableDevices;
    }

}
