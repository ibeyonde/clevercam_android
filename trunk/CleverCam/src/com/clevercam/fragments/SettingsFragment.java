/**
 * 
 */
package com.clevercam.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.clevercam.R;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.views.materialdialogs.MaterialDialog;


/**
 * @author Shashi
 *
 */
public class SettingsFragment extends BaseFragment implements AdapterView.OnItemClickListener {

	private final static String TAG = SettingsFragment.class.getName();
	
	private  static final String[] SETTINGS_LIST_ITEMS = new String[] { "Device Profile" };
	
	private ListView mSettingsListView;

	public SettingsFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
		setIdsToViews(rootView);
		setAdapter();
        setClickistner();
		return rootView;
	}

    private void setClickistner() {
        mSettingsListView.setOnItemClickListener(this);
    }

    private void setAdapter() {
		mSettingsListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.row_list_itmes, SETTINGS_LIST_ITEMS));
	}

	private void setIdsToViews(View rootView) {
		mSettingsListView = (ListView)rootView.findViewById(R.id.settings_lv);
	}


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CleverCamLogger.infoLog(TAG, "List view item is clicked : "+position);
        switch (position){
            case 0:
                showCameraDetailsDialog();
            break;
        }
    }

    private void showCameraDetailsDialog() {
        final  View positiveAction;
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.device_details_dialog_title)
                .customView(R.layout.device_profile_dialog)
                .positiveText(R.string.dialog_Ok)
                .negativeText(android.R.string.cancel)
                .callback(new MaterialDialog.Callback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        dialog.dismiss();
                        DeviceManager.getInstance().setDeviceName(((EditText)dialog.getCustomView().findViewById(R.id.device_profile_device_name_et)).getText().toString());
                    }
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        dialog.dismiss();
                    }
                }).build();
        dialog.show();
        View customView = dialog.getCustomView();
        EditText deviceName = (EditText)customView.findViewById(R.id.device_profile_device_name_et);
        deviceName.setText(DeviceManager.getInstance().getDeviceName());
        ((Button)dialog.getCustomView().findViewById(R.id.device_profile_clear_data_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeviceManager.getInstance().clearDeviceData();
                showShortToast("Data is cleard!");
            }
        });
    }
}
