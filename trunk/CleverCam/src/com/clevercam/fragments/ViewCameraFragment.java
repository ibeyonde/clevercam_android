/**
 * 
 */
package com.clevercam.fragments;

import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.VideoView;

import com.clevercam.R;
import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.model.AvailableDevicesModel;
import com.clevercam.base.model.ViewCameraModel;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.views.materialdialogs.MaterialDialog;

/**
 * This fragment will show all the the video feeds which are connected in to the device
 * @author Shashi
 *
 */
public class ViewCameraFragment extends BaseFragment {

	private final static String TAG = ViewCameraFragment.class.getName();
    private VideoView mPrimaryVideoFeedView;
    private List<AvailableDevicesModel> mAvailableDevicesList;
    private String[] mRemoteIPArray;

    public ViewCameraFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        init();
        getBundleData();
		View rootView = inflater.inflate(R.layout.view_camera_layout, container, false);
        setIdsToViews(rootView);
        showListOfDevicesAvaialbleDialog();
		return rootView;
	}

    private void showListOfDevicesAvaialbleDialog() {
        ArrayAdapter <String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, mRemoteIPArray);
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Available devices")
                .contentColorRes(android.R.color.black)
                .adapter(arrayAdapter)
                .build();

        ListView listView = dialog.getListView();
        if (listView != null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                
                    startPrimaryCamera((String)parent.getItemAtPosition(position));
                    dialog.dismiss();
                }
            });
        }

        dialog.show();
    }

    private void init() {

    }

    private void startPrimaryCamera(String ip) {
        CleverCamLogger.infoLog(TAG, "URL ---------> "+String.format("http://%s:7567/?action=stream", ip));
        mPrimaryVideoFeedView.setVideoURI(Uri.parse(String.format("http://%s:7567/?action=stream",ip)));
        mPrimaryVideoFeedView.start();
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if(bundle != null) {
            ViewCameraModel cameraModel = (ViewCameraModel) bundle.getSerializable(CleverCamConstants.AVILABLE_DEVICE_LIST_KEY);
            mAvailableDevicesList = cameraModel.availableDeviceList;

        }else{
            mAvailableDevicesList = CleverCamDataHolder.getInstance().getAvailableDevices();
        }
        mRemoteIPArray = new String[mAvailableDevicesList.size()];
        for(int i=0 ; i< mAvailableDevicesList.size() ; i++){
            AvailableDevicesModel availDevices = mAvailableDevicesList.get(i);
            mRemoteIPArray[i] = availDevices.remoteIP;
            CleverCamLogger.infoLog(TAG, "Device IP : "+availDevices.remoteIP +"\n Device Name : "+availDevices.deviceName +"\n Capabilities : "+availDevices.capabilities);
        }
    }

    /**
     * Setting ids to respective views
     * @param rootView
     */
    private void setIdsToViews(View rootView) {
        mPrimaryVideoFeedView = (VideoView) rootView.findViewById(R.id.view_camera_main_video_stream_video_view);
    }
}
