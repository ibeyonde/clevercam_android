/**
 * 
 */
package com.clevercam.fragments;

import java.util.ArrayList;
import java.util.List;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.clevercam.R;
import com.clevercam.activities.ViewCameraActivity;
import com.clevercam.base.CleverCamDataHolder;
import com.clevercam.base.api.AsyncAPIResponseHandler;
import com.clevercam.base.api.model.ResponseResult;
import com.clevercam.base.managers.DeviceManager;
import com.clevercam.base.managers.IDeviceManager;
import com.clevercam.base.model.AvailableDevicesModel;
import com.clevercam.base.model.ViewCameraModel;
import com.clevercam.base.model.WiFiConfigModel;
import com.clevercam.base.network.ConnectionManager;
import com.clevercam.utils.CleverCamConstants;
import com.clevercam.utils.CleverCamLogger;
import com.clevercam.utils.views.RippleBackground;
import com.clevercam.views.materialdialogs.DialogAction;
import com.clevercam.views.materialdialogs.MaterialDialog;

/**
 * Fragment to discover the device and connect to WiFi mode
 * @author Shashi
 *
 */
public class DiscoverDeviceFragment extends BaseFragment implements View.OnClickListener {

	private final static String TAG = DiscoverDeviceFragment.class.getName();

	private RippleBackground mRippleBackground;
	private Handler mRippleAnimlHandler;
	private ImageView mFoundDeviceIV;
	private Handler mAPIHandler;
	private EditText mWifiasswordInput = null;
	private EditText mSSID = null;
	private ImageView mStartBroadcastIV;
	private EditText staticIPET = null;
	private EditText gatewayET = null;
	private EditText netmaskET = null;

	public DiscoverDeviceFragment() {
		// Empty constructor required for fragment subclasses
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.discover_layout, container, false);
		setIdsToViews(rootView);
		init();
		startRippleAnimation();
		setUpListners();
		return rootView;
	}

	private void setUpListners() {
		mStartBroadcastIV.setOnClickListener(this);
	}

	private void setAPIHandler() {
		mAPIHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				int responseCode = 0;
				if(msg != null) {    //MESSAGE IS NOT NULL lets get the response results which is returned by API
					IDeviceManager deviceManager = DeviceManager.getInstance();
					ResponseResult respResult = (ResponseResult)msg.obj;
					responseCode = respResult.statusCode;
					showShortToast("Response from server - " + responseCode);
					if (responseCode == CleverCamConstants.RESPONSE_SUCCESS || responseCode == 0) {
						ConnectionManager.getInstance().connectToWiFi(mSSID.getText().toString());
						
						DeviceManager.getInstance().setDeviceConfigurationState(true);   //This method will set the state of the device connected to wifi true
						if(!deviceManager.isDeviceConfigured()) {
							mRippleBackground.setVisibility(View.GONE);
							mStartBroadcastIV.setVisibility(View.VISIBLE);
						}
						
						startBroadcastingForUDPPackets();
						showShortToast("Please wait for a while to discover the ip from WIFI..... ");
						
					}
				}
			}
		};
		AsyncAPIResponseHandler.getInstance().setUIHandler(mAPIHandler);
	}
	
	/**
	 * Method to start broadcast for UDP packets on 6999 port this should
	 * be called only once the device is connected to Home Wifi
	 */
	private void startBroadcastingForUDPPackets() {
		CountDownTimer ct = new CountDownTimer(30000, 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
			}

			@Override
			public void onFinish() {
				new ReceiveBroadcastTask().execute();
			}
		}.start();
	}

	private void init() {
		mRippleAnimlHandler = new Handler();
		discoverCleverCam();

	}

	/**
	 * Method to discover Clevercam
	 */
	private void discoverCleverCam() {
		CleverCamLogger.infoLog(TAG, "discoverCleverCam() :: discovering Clevercam..");
		if(!DeviceManager.getInstance().isDeviceConfigured()) {              // IF the device is not configured
			boolean isDeviceConnectedInAdHocMode = DeviceManager.getInstance().connectInAdhocMode();
			if (isDeviceConnectedInAdHocMode) {
				CleverCamLogger.infoLog(TAG, "discoverCleverCam() :: Device is connected in Adhoc mode.");
				mFoundDeviceIV.setVisibility(View.VISIBLE);
				showWifiConfigureDialog();
			} else {
				showShortToast(getString(R.string.error_connecting_clevercam_adhoc_mode));
				mFoundDeviceIV.setVisibility(View.GONE);
				mRippleBackground.stopRippleAnimation();   // As we clever cam is not discoverd we are stopping the animation
			}
		}else{   // Its configured with Wifi not broad casted
			showShortToast("Trying to receive UDP packets on 6999 port");
			CleverCamLogger.infoLog(TAG, "discoverCleverCam() :: Trying to receive UDP packets on 6999 port");
			mFoundDeviceIV.setVisibility(View.VISIBLE);
			startBroadcastingForUDPPackets();
		}
	}

	private void showWifiConfigureDialog() {
		final  View positiveAction;
		
		ConnectionManager connManager = ConnectionManager.getInstance() ;
		MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
		.title(R.string.configure_wifi_dialog_title)
		.customView(R.layout.configure_wifi_dialog)
		.positiveText(R.string.configure_wifi_dialog_connect)
		.negativeText(android.R.string.cancel)
		.callback(new MaterialDialog.Callback() {
			@Override
			public void onPositive(MaterialDialog dialog) {
				showShortToast(mWifiasswordInput.getText().toString());
				dialog.dismiss();
				String ssid = mSSID.getText().toString();
				if (!ssid.isEmpty()) {
					final String wifiPassword = mWifiasswordInput.getText().toString();
					WiFiConfigModel wifiDetailsModel = new WiFiConfigModel();
					wifiDetailsModel.ssid = ssid;
					wifiDetailsModel.password = wifiPassword;
					wifiDetailsModel.gateway = gatewayET.getText().toString();
					wifiDetailsModel.netmask = netmaskET.getText().toString();
					wifiDetailsModel.staticIP = staticIPET.getText().toString();
					DeviceManager.getInstance().sendWiFIDetailsToDeviceByStaticIP(wifiDetailsModel, mAPIHandler);
					showShortToast("Please wait for a minute to configure Wifi..... ");
							setAPIHandler();
							dialog.setCancelable(false);
				} else {
					showShortToast(getString(R.string.configure_wifi_dialog_ssid_text));
				}
			}
			@Override
			public void onNegative(MaterialDialog dialog) {
				dialog.dismiss();
			}
		}).build();

		positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
		mWifiasswordInput = (EditText) dialog.getCustomView().findViewById(R.id.configure_wifi_dialog_pwd_et);
		staticIPET = (EditText)dialog.getCustomView().findViewById(R.id.configure_wifi_dialog_static_ip_et);
		netmaskET = (EditText)dialog.getCustomView().findViewById(R.id.configure_wifi_dialog_netmask_et);
		gatewayET = (EditText)dialog.getCustomView().findViewById(R.id.configure_wifi_dialog_gateway_et);
		mSSID = (EditText) dialog.getCustomView().findViewById(R.id.configure_wifi_dialog_ssid_et);

		//Setting config details
		gatewayET.setText(connManager.getGateway());
		netmaskET.setText(connManager.getNetmask());
		staticIPET.setText(DeviceManager.getInstance().getStaticIPAssignedForMasterDevice());
		mSSID.setText(DeviceManager.getInstance().getSSID());


		mWifiasswordInput.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				//positiveAction.setEnabled(s.toString().trim().length() > 0);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		// Toggling the show password CheckBox will mask or unmask the password input EditText
		((CheckBox) dialog.getCustomView().findViewById(R.id.showPassword)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mWifiasswordInput.setInputType(!isChecked ? InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
				mWifiasswordInput.setTransformationMethod(!isChecked ? PasswordTransformationMethod.getInstance() : null);
			}
		});
		dialog.show();
		positiveAction.setEnabled(true); // disabled by default
	}



	private void setIdsToViews(View view) {
		mRippleBackground = (RippleBackground)view.findViewById(R.id.ripple_holder);
		mFoundDeviceIV    = (ImageView)view.findViewById(R.id.foundDevice);
		mStartBroadcastIV = (ImageView)view.findViewById(R.id.send_broadcast_iv);
	}

	public void startRippleAnimation(){
		mRippleAnimlHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				mRippleBackground.startRippleAnimation();
				AnimatorSet animatorSet = new AnimatorSet();
				animatorSet.setDuration(400);
				animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
				ArrayList<Animator> animatorList = new ArrayList<Animator>();
				ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(mFoundDeviceIV, "ScaleX", 0f, 1.2f, 1f);
				animatorList.add(scaleXAnimator);
				ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(mFoundDeviceIV, "ScaleY", 0f, 1.2f, 1f);
				animatorList.add(scaleYAnimator);
				animatorSet.playTogether(animatorList);
				animatorSet.start();
			}
		}, 1000);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
		case R.id.send_broadcast_iv:
			mRippleBackground.setVisibility(View.VISIBLE);
			mStartBroadcastIV.setVisibility(View.GONE);
			startBroadcastingForUDPPackets();
			startRippleAnimation();
			break;
		}
	}


	private class ReceiveBroadcastTask extends AsyncTask<Object, Object, List<String>> {

		@Override
		protected void onPreExecute() {
			showShortToast("Searching for broadcast packets!.. ");
		}

		protected List<String> doInBackground(Object... arg0) {
			List<String> receivedPacker = null;
			receivedPacker = ConnectionManager.getInstance().receiveUDPPackets();   // Listen to 6999 port with UDP packets
			return receivedPacker;
		}

		protected void onPostExecute(List<String> receivedPacketDataList) {
			if(receivedPacketDataList.size() == 0 || receivedPacketDataList.size() == 1){
				showShortToast("Timed out !! , please retry");
				mRippleBackground.setVisibility(View.GONE);
				mStartBroadcastIV.setVisibility(View.VISIBLE);
				startBroadcastingForUDPPackets();
			}else{
				showShortToast("Number of broadcast messages received -> "+receivedPacketDataList.size());
				CleverCamLogger.infoLog(TAG, "onPostExecute() :: Received packets -- > "+receivedPacketDataList);
				List<AvailableDevicesModel> availableDevicesListModels = new ArrayList<AvailableDevicesModel>();
				for(String recivedData : receivedPacketDataList){
					System.out.println("Recived packets $$$$$$ "+recivedData);
					if(recivedData.contains(CleverCamConstants.CLEVER_CAM_BROADCAST_IDENTIFIER)){
						CleverCamLogger.infoLog(TAG, "onPostExecute() :: Received data XXXXXXXXXXXXXXX..... "+recivedData);
						AvailableDevicesModel deviceModel = new AvailableDevicesModel();
						String [] splitString 	= recivedData.split(CleverCamConstants.COLON);
						deviceModel.remoteIP 	= splitString[0].substring(1);
						deviceModel.deviceName 	= splitString[1];  // get IP /
						deviceModel.capabilities = splitString[2].split(CleverCamConstants.COMMA);
						availableDevicesListModels.add(deviceModel);
					}
				}
				ViewCameraModel viewCameraModel = new ViewCameraModel();
				viewCameraModel.availableDeviceList = availableDevicesListModels;
				CleverCamDataHolder.getInstance().setAvaialbleDeviceList(availableDevicesListModels);
				Intent viewCameraIntent = new Intent(getActivity(), ViewCameraActivity.class);
				getActivity().startActivity(viewCameraIntent);
			}
		}
	}
}
