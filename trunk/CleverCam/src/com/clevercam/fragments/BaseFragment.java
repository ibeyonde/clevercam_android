/**
 * 
 */
package com.clevercam.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.Toast;

import com.clevercam.R;
import com.clevercam.utils.CleverCamLogger;


/**
 * @author Shashi
 *
 */
public class BaseFragment extends Fragment  {

	private static String TAG = BaseFragment.class.getName();

	/**
	 * To navigate to other fragment, to keep fragment in back stack pass the second parameter
	 * @param fragment
	 * @param keepFragmentInBackStack
	 */
	public void launchFragment(Fragment fragment, boolean keepFragmentInBackStack) {
		CleverCamLogger.infoLog(TAG, "Launching fragment:: "+ fragment.getClass().getSimpleName());
		FragmentManager fragmentManager = getFragmentManager();
		if(fragmentManager!=null){
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fragmentTransaction.replace(R.id.container, fragment, fragment.getClass().getName());
			if (keepFragmentInBackStack) fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName()); 
			// app is will crash with commit operation after savedInstanceState.
			// http://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa

			// FIXME : need to identify the best solution between
			// commitAllowingStateLoss VS removing super.onSaveInstanceState in
			// onSaveInstanceState
			fragmentTransaction.commitAllowingStateLoss();
		}
	}

    /**
     * Method to show short toast
     * @param toastString
     */
    protected void showShortToast(String toastString){
        Toast.makeText(getActivity(), toastString, Toast.LENGTH_SHORT).show();
    }

    public void onPause() {

        super.onPause();
    }

    /**
     * Method to show Long toast
     * @param toastString
     */
    protected void showLongToast(String toastString){
        Toast.makeText(getActivity(), toastString, Toast.LENGTH_SHORT).show();
    }

}
