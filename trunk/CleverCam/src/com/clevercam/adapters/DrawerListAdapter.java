/**
 * 
 */
package com.clevercam.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clevercam.R;
import com.clevercam.adapters.holders.DrawerItemHolder;

/**
 * @author Shashi
 *
 */
public class DrawerListAdapter extends BaseAdapter {

	private LayoutInflater mLayoutInflater;
	private Context mContext;
	private List<String> mDrawerListItems;

	public DrawerListAdapter(Context context, List<String> drawerItems) {
		this.mContext = context;
		this.mDrawerListItems = drawerItems;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}


	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return mDrawerListItems.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return mDrawerListItems.get(position);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = null;
		row = convertView == null ? mLayoutInflater.inflate(R.layout.drawer_list_item, parent, false) : convertView;
		DrawerItemHolder drawerItemHolder = new DrawerItemHolder();
		drawerItemHolder.drawerTitle = (TextView) (row.findViewById(R.id.drawer_list_item_title_tv));
		drawerItemHolder.drawerTitle.setText((String)getItem(position));
		return row;
	}
}
